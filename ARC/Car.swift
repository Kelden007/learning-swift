struct Car {
    var brand: String
    var year: Int
    var trunkVolume: Double
    var isEngineOn: Bool
    var areWindowsOpen: Bool
    var filledTrunkVolume: Double
    
    mutating func performAction(action: CarAction) {
        switch action {
        case .startEngine:
            isEngineOn = true
        case .stopEngine:
            isEngineOn = false
        case .openWindows:
            areWindowsOpen = true
        case .closeWindows:
            areWindowsOpen = false
        case .loadCargo(let cargoVolume):
            filledTrunkVolume += cargoVolume
            if filledTrunkVolume > trunkVolume {
                filledTrunkVolume = trunkVolume
            }
        case .unloadCargo(let cargoVolume):
            filledTrunkVolume -= cargoVolume
            if filledTrunkVolume < 0 {
                filledTrunkVolume = 0
            }
        }
    }
}

struct Truck {
    var brand: String
    var year: Int
    var bodyVolume: Double
    var isEngineOn: Bool
    var areWindowsOpen: Bool
    var filledBodyVolume: Double
    
    mutating func performAction(action: TruckAction) {
        switch action {
        case .startEngine:
            isEngineOn = true
        case .stopEngine:
            isEngineOn = false
        case .openWindows:
            areWindowsOpen = true
        case .closeWindows:
            areWindowsOpen = false
        case .loadCargo(let cargoVolume):
            filledBodyVolume += cargoVolume
            if filledBodyVolume > bodyVolume {
                filledBodyVolume = bodyVolume
            }
        case .unloadCargo(let cargoVolume):
            filledBodyVolume -= cargoVolume
            if filledBodyVolume < 0 {
                filledBodyVolume = 0
            }
        }
    }
}
