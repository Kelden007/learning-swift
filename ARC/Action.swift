enum CarAction {
    case startEngine
    case stopEngine
    case openWindows
    case closeWindows
    case loadCargo(Double)
    case unloadCargo(Double)
}

enum TruckAction {
    case startEngine
    case stopEngine
    case openWindows
    case closeWindows
    case loadCargo(Double)
    case unloadCargo(Double)
}
