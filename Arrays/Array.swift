// Создаем массив количества дней в месяцах
let daysInMonths = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]

// Создаем массив названий месяцев
let monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]

// Выводим количество дней в каждом месяце
for days in daysInMonths {
    print(days)
}

// Выводим название месяца и количество дней
for (index, month) in monthNames.enumerated() {
    print("\(month) has \(daysInMonths[index]) days")
}

// Создаем массив tuples с параметрами (имя месяца, кол-во дней)
let monthData = [("January", 31), ("February", 28), ("March", 31), ("April", 30), ("May", 31), ("June", 30), ("July", 31), ("August", 31), ("September", 30), ("October", 31), ("November", 30), ("December", 31)]

// Выводим название месяца и количество дней из массива tuples
for data in monthData {
    print("\(data.0) has \(data.1) days")
}

// Выводим количество дней в каждом месяце в обратном порядке
for days in daysInMonths.reversed() {
    print(days)
}

// Вычисляем количество дней до произвольной даты от начала года
let inputMonth = 4
let inputDay = 15
var daysCount = 0

for i in 0..<inputMonth {
    daysCount += daysInMonths[i]
}

daysCount += inputDay - 1

print("There are \(daysCount) days from the beginning of the year to \(inputMonth)/\(inputDay)")
