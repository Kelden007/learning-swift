struct IOSCollection<Element> {
    private var elements: [Element]
    
    init(_ elements: [Element] = []) {
        self.elements = elements
    }
    
    mutating func append(_ element: Element) {
        copyIfNeeded()
        elements.append(element)
    }
    
    private mutating func copyIfNeeded() {
        if !isKnownUniquelyReferenced(&elements) {
            elements = elements.copy()
        }
    }
}

extension Array {
    func copy() -> Array {
        return self
    }
}
