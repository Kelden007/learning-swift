let name = "Denis"
let type = "Хомо сапиенс"
let language = "Swift"
let knowledgeCutoff = "2023-03"
let currentDate = "2023-04-01"

let summary = "Меня зовут Денис, я являюсь гражданином мира, живущем и развивающимся на основе вида \(type). Я изучаю программирование на языке \(language) и получаю знания по состоянию на \(knowledgeCutoff). Сегодня \(currentDate) я также изучаю новые проекты."

print(summary)
