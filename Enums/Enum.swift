// Создайте по 2 enum с разным типом RawValue
enum Direction: Int {
case north = 0
case east
case south
case west
}

enum Weekday: String {
case sunday
case monday
case tuesday
case wednesday
case thursday
case friday
case saturday
}

// Создайте несколько enum для заполнения полей стркутуры - анкета сотрудника
enum Gender {
case male
case female
case nonBinary
}

enum AgeCategory {
case young
case middleAged
case elderly
}

enum Experience {
case beginner
case intermediate
case advanced
}

// Создать enum со всеми цветами радуги
enum RainbowColor {
case red
case orange
case yellow
case green
case blue
case indigo
case violet
}

// Создать функцию, которая содержит массив разных case'ов enum и выводит содержимое в консоль
func printEnumCases() {
enum Fruits {
case apple
case banana
case cherry
case durian
}

enum Planets {
    case mercury
    case venus
    case earth
    case mars
    case jupiter
    case saturn
    case uranus
    case neptune
}

enum Colors {
    case red
    case blue
    case green
    case yellow
}

let fruits = Fruits.apple
let planets = Planets.earth
let colors = Colors.red

print("\(fruits) \(colors), \(planets) planet")

}

printEnumCases() // Output: "apple red, earth planet"

// Создать функцию, которая выставляет оценки ученикам в школе, на входе принимает значение enum Score: String {<Допишите case'ы} и выводит числовое значение оценки
enum Score: String {
case A = "Excellent"
case B = "Good"
case C = "Satisfactory"
case D = "Needs Improvement"
case F = "Fail"
}

func getScoreValue(score: Score) -> Int {
switch score {
case .A:
return 5
case .B:
return 4
case .C:
return 3
case .D:
return 2
case .F:
return 1
}
}

let myScore = Score.A
print(getScoreValue(score: myScore)) // Output: 5

// Создать метод, которая выводит в консоль какие автомобили стоят в гараже, используйте enum
enum Car {
case sedan
case suv
case sportsCar
case truck
}

func listCarsInGarage() {
print("Cars in the garage:")
print("(Car.sedan)")
print("(Car.suv)")
print("(Car.sportsCar)")
print("(Car.truck)")
}

listCarsInGarage()
// Output:
// Cars in the garage:
// sedan
// suv
// sportsCar
// truck