// Родительский класс
class Vehicle {
    var brand: String
    var model: String
    
    init(brand: String, model: String) {
        self.brand = brand
        self.model = model
    }
    
    func drive() {
        print("Driving...")
    }
}

// Дочерний класс 1
class Car: Vehicle {
    var numWheels: Int
    
    init(brand: String, model: String, numWheels: Int) {
        self.numWheels = numWheels
        super.init(brand: brand, model: model)
    }
    
    override func drive() {
        print("Driving a car...")
    }
}

// Дочерний класс 2
class Bike: Vehicle {
    var numGears: Int
    
    init(brand: String, model: String, numGears: Int) {
        self.numGears = numGears
        super.init(brand: brand, model: model)
    }
    
    override func drive() {
        print("Riding a bike...")
    }
}
