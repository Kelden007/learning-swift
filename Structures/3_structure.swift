class Student {
    var name: String
    var grade: Int
    var score: Double
    
    init(name: String, grade: Int, score: Double) {
        self.name = name
        self.grade = grade
        self.score = score
    }
}

class StudentManager {
    func sortByName(_ students: [Student]) -> [Student] {
        return students.sorted { $0.name < $1.name }
    }
    
    func sortByGrade(_ students: [Student]) -> [Student] {
        return students.sorted { $0.grade < $1.grade }
    }
    
    func sortByScore(_ students: [Student]) -> [Student] {
        return students.sorted { $0.score > $1.score }
    }
}

let student1 = Student(name: "John", grade: 11, score: 85.0)
let student2 = Student(name: "Mary", grade: 10, score: 92.5)
let student3 = Student(name: "Bob", grade: 12, score: 77.0)

let students = [student1, student2, student3]

let manager = StudentManager()

let studentsByName = manager.sortByName(students)
print("Students sorted by name:")
for student in studentsByName {
    print(student.name)
}
// Output:
// Students sorted by name:
// Bob
// John
// Mary

let studentsByGrade = manager.sortByGrade(students)
print("Students sorted by grade:")
for student in studentsByGrade {
    print(student.name)
}
// Output:
// Students sorted by grade:
// Mary
// John
// Bob

let studentsByScore = manager.sortByScore(students)
print("Students sorted by score:")
for student in studentsByScore {
    print(student.name)
}
// Output:
// Students sorted by score:
// Mary
// John
// Bob

