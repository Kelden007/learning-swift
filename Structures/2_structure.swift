class House {
    var width: Double
    var height: Double
    
    init(width: Double, height: Double) {
        self.width = width
        self.height = height
    }
    
    func create() {
        let area = width * height
        print("The house has an area of \(area) square units.")
    }
    
    func destroy() {
        print("The house has been destroyed.")
    }
}

let myHouse = House(width: 10, height: 20)
myHouse.create() // The house has an area of 200.0 square units.
myHouse.destroy() // The house has been destroyed.
